# Base image for emulating android devices

From https://github.com/google/android-emulator-container-scripts

Currently this builds periodicaly some emulators (see the container
registry for the exact list).

# Run it

```
docker run \
  -e "ADBKEY=$(cat ~/.android/adbkey)" --device /dev/kvm --publish \
  8554:8554/tcp --publish 15555:5555/tcp  \
  registry.gitlab.inria.fr/android-tools/emulator/q-google-x64:30.0.12
```

# Running on Igrida/Grid'5000 using singularity

Note: KVM needs to be installed, the newer hardware, the better
Note: debugger port (adb) is available on port 5555 (even remotely :))
## Grid'5000

```
# Grid'5000
frontend) oarsub -I -l "nodes=1,walltime=01:00:00"

node) module load singularity/3.5.2_gcc-8.3.0
# build the singularity environment ()
node) singularity build --sandbox q-google-x64  docker://registry.gitlab.inria.fr/android-tools/emulator/q-google-x64:30.0.12
node) singularity run --writable --pwd /android/sdk q-google-x64
```

## Igrida

emulator.sh:
```
#!/bin/bash

#OAR -l nodes=1, walltime=02:00:00
#OAR -p virt='YES' and chassis='igrida04'

set -x

FLAVOUR=o-google-x64

source /etc/profile.d/modules.sh
module load spack/singularity

export SINGULARITY_CACHEDIR=/srv/tempdd/${USER}/.singularity
mkdir -p $SINGULARITY_CACHEDIR

mkdir -p /srv/tempdd/${USER}
cd /srv/tempdd/${USER}

ls ${FLAVOUR} || singularity build --sandbox ${FLAVOUR} docker://registry.gitlab.inria.fr/android-tools/emulator/${FLAVOUR}:30.0.12
singularity run --writable --pwd /android/sdk ${FLAVOUR}
```

- Launch it: `oarsub -S ./emulator.sh`
- From you local machine you should be able to launch adb: `adb connect <node>:5555`.

## Limitations on Igrida / usermode

Due *at least* to port collisions only one emulator can run on a given node.
But, a quick and dirty/fix would be to change the ports in `launch_emulator.sh` (emulator ports and grpc + socat)
(Réf: https://github.com/google/android-emulator-container-scripts/issues/189)

